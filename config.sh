#!/bin/bash

# Updating the package database
sudo apt-get update

# Installing necessary packages for Docker installation
sudo apt-get install -y ca-certificates curl gnupg lsb-release

# Adding Docker's official GPG key:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# Setting up the Docker repository:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Updating the package database with the Docker packages from the newly added repo
sudo apt-get update

# Installing Docker Engine
sudo apt-get install -y docker-ce docker-ce-cli containerd.io

# Verifying Docker Installation
docker --version

# Setting up the repository for Docker Compose
sudo apt-get update

# Installing Docker Compose
sudo apt-get install -y docker-compose-plugin

# Verifying Docker Compose Installation
docker compose version


#!/bin/bash

# Step 1: Attach the Block Volume to the Instance
# Note: You would typically do this via the OCI console or CLI

# Step 2: Identify the Block Volume Device Path
# The device path may vary, but it's typically something like /dev/sdb
BLOCK_DEVICE_PATH="/sdb"

# Step 3: Format the Block Volume (if it's not already formatted)
sudo mkfs.ext4 $BLOCK_DEVICE_PATH

# Step 4: Create a Mount Point
MOUNT_POINT="/mnt/neo4j-data"
sudo mkdir -p $MOUNT_POINT

# Step 5: Mount the Block Volume
sudo mount $BLOCK_DEVICE_PATH $MOUNT_POINT

# Step 6: Set appropriate permissions
# Replace 'username' and 'group' with your username and group
sudo chown root:root $MOUNT_POINT

# Step 7: Update /etc/fstab to ensure the block volume mounts on instance reboot
echo "$BLOCK_DEVICE_PATH $MOUNT_POINT ext4 defaults,nofail 0 2" | sudo tee -a /etc/fstab

# Step 8: Run the Neo4j Docker container
# Note: The --env=NEO4J_AUTH=neo4j/neo4j option sets the default login credentials.
# You should replace 'neo4j/neo4j' with your own credentials or use a different
# method to secure your Neo4j instance.

echo "n-L48gnn#Ozp3ZhmnsUY" | docker login us-phoenix-1.ocir.io --username axnlpvk9gkaa/minal2 --password-stdin && docker pull us-phoenix-1.ocir.io/axnlpvk9gkaa/database:latest
echo "n-L48gnn#Ozp3ZhmnsUY" | docker login us-phoenix-1.ocir.io --username axnlpvk9gkaa/minal2 --password-stdin && docker pull us-phoenix-1.ocir.io/axnlpvk9gkaa/api:latest
echo "n-L48gnn#Ozp3ZhmnsUY" | docker login us-phoenix-1.ocir.io --username axnlpvk9gkaa/minal2 --password-stdin && docker pull us-phoenix-1.ocir.io/axnlpvk9gkaa/frontend:latest

sudo docker compose up





