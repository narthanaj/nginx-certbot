#!/bin/bash

# Check if docker-compose is installed
if ! [ -x "$(command -v docker-compose)" ]; then
  echo 'Error: docker-compose is not installed.' >&2
  exit 1
fi

# Define domains, key size, data path, email, and staging flag
domains=("companiaa.online" "www.companiaa.online")
rsa_key_size=4096
data_path="./data/certbot"
email="narthanaj@example.com"  # Replace with your email address
staging=0  # Set to 1 if testing, 0 for production

# Check if data directory exists and delete if found
if [ -d "$data_path" ]; then
  read -p "Existing data found for ${domains[*]}. Do you want to remove it and continue? (y/N) " decision
  if [[ "$decision" =~ ^[Yy]$ ]]; then
    rm -rf "$data_path"
  else
    exit
  fi
fi

# Stop and remove existing Docker containers and images for the project
echo "### Stopping and removing existing Docker containers and images for the project ..."
docker-compose down --volumes --remove-orphans
docker rmi $(docker images -q)

# Download recommended TLS parameters
echo "### Downloading recommended TLS parameters ..."
mkdir -p "$data_path/conf"
curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/_internal/tls_configs/options-ssl-nginx.conf > "$data_path/conf/options-ssl-nginx.conf"
curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot/certbot/ssl-dhparams.pem > "$data_path/conf/ssl-dhparams.pem"
echo

echo "### Creating dummy certificate for ${domains[*]} ..."
path="/etc/letsencrypt/live/${domains[0]}"
mkdir -p "$data_path/conf/live/${domains[0]}"
docker-compose run --rm --entrypoint "\
  openssl req -x509 -nodes -newkey rsa:$rsa_key_size -days 1\
    -keyout '$path/privkey.pem' \
    -out '$path/fullchain.pem' \
    -subj '/CN=localhost'" certbot
echo

echo "### Starting nginx ..."
docker-compose up -d

echo "### Requesting Let's Encrypt certificate for ${domains[*]} ..."
# Join domains to -d args
domain_args=""
for domain in "${domains[@]}"; do
  domain_args="$domain_args -d $domain"
done

# Select appropriate email arg
if [ -n "$email" ]; then
  email_arg="--email $email"
else
  email_arg="--register-unsafely-without-email"
fi

# Enable staging mode if needed
if [ "$staging" -ne 0 ]; then
  staging_arg="--staging"
else
  staging_arg=""
fi

docker-compose run --rm --entrypoint "\
  certbot certonly --webroot -w /var/www/certbot \
    $staging_arg \
    $email_arg \
    $domain_args \
    --rsa-key-size $rsa_key_size \
    --agree-tos \
    --force-renewal" certbot
echo

echo "### Reloading nginx ..."
docker-compose exec nginx nginx -s reload
